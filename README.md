<h2 align="center">Project Dibuat Menggunakan Angular 14 dengan SCSS.</h1>
<p align="center">
  <br>
  <i>Build with Love by <a href='https://wa.me/6285885881998'>Naufal Ihsan</a></i><br>
  <i>Project ini dapat dilihat secara live melalui Vercel <a href='https://angular-nopal98.vercel.app/' target="_blank">pada laman ini</a></i>
  <br>
</p>
<p align="center">
  <a href="https://nopal.my.id"><strong>www.nopal.my.id</strong></a>
  <br>
</p>
<hr />

### Tech Stack

- Angular v14 [![Love Angular badge](https://img.shields.io/badge/angular-love-blue?logo=angular&angular=love)](https://www.github.com/angular/angular)
- Bootstrap v5
- SCSS
- Stackoverflow 🥰

### Setting Up a Project

1. Pertama-tama, pastikan Node.js sudah terinstal di komputer Anda. Jika belum, silakan unduh dan instal Node.js dari situs resminya (<a href="https://nodejs.org/"><strong>www.nodejs.org</strong></a>)
2. Buka terminal atau command prompt di komputer Anda, lalu masuk ke direktori di mana Anda ingin menyimpan project Angular Ini.
3. Lakukan clone repository menggunakan perintah git clone.

```
git clone https://github.com/nopal98/angular.git
```

4. Setelah clone selesai, masuk ke direktori project tersebut.

```
cd angular
```

5. Jalankan perintah npm install untuk menginstal paket-paket yang dibutuhkan oleh project Angular.

```
npm install
```

6. Jika ada dependensi yang tidak terinstal, jalankan perintah npm install -g @angular/cli untuk menginstal Angular CLI secara global.

```
npm install -g @angular/cli
```

7. <strong>Jika Terdapat Error ketika NPM Install</strong>, maka lakukan perintah dibawah ini

```
npm i --legacy-peer-deps
```

8. Setelah semua paket selesai terinstal, jalankan perintah dibawah ini untuk menjalankan aplikasi.

```
ng serve -o
```

<hr />
