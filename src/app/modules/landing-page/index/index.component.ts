import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { PackageService } from 'src/app/core/services/package/package.service'

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {

  packages$: Observable<any>

  constructor(
    private router: Router,
    private packageService: PackageService 
  
    ) { }

  ngOnInit(): void {
    // this.packages$ = this.packageService.getAll$()
    this.router.navigate(['auth/login']);
  }

}
