import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from '@angular/forms';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { IndexComponent } from './index/index.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from "../../shared/shared.module";
import { ListComponent } from './list/list.component';
import {ClipboardModule} from '@angular/cdk/clipboard';
import { NgChartsModule } from 'ng2-charts';
import {TableModule} from 'primeng/table';
import {SkeletonModule} from 'primeng/skeleton';
import { CalendarModule } from 'primeng/calendar';
import {InputNumberModule} from 'primeng/inputnumber';
import { CreateComponent } from './create/create.component';
import {DropdownModule} from 'primeng/dropdown';
import {ToastModule} from 'primeng/toast';
import { DetailComponent } from './detail/detail.component';

@NgModule({
  declarations: [
    IndexComponent,
    ListComponent,
    CreateComponent,
    DetailComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    NgbModule,
    SharedModule,
    NgChartsModule,
    ClipboardModule,
    TableModule,
    SkeletonModule,
    CalendarModule,
    FormsModule,
    InputNumberModule,
    DropdownModule,
    ToastModule
  ]
})
export class DashboardModule { }
