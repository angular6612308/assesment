import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import {EmployeesService} from 'src/app/core/services/employees/employees.service';
import { MessageService } from 'primeng/api';
@Component({
  selector: '[detail]',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  public form: FormGroup;
  public isValidURL:string
  public idQuery:string
  constructor(
    private messageService: MessageService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private employeeService: EmployeesService,
    private router: Router
  ) {
    this.form = this.fb.group({
      username: [null],
      firstName: [null],
      lastName: [null],
      email: [null],
      birthDate: [null],
      basicSalary: [null],
      status: [null],
      group: [null],
      age:[null],
      description: [null]
    });
  }

  ngOnInit(): void {
    this.route.queryParams.subscribe({
      next: (params) => {
       this.isValidURL = params.detail
       this.getData(params.id)
      },
    });
    
  }

  cancel() {
    this.router.navigate(['/admin/dashboard/index']);
  }

  getData(id: string) {
    // Ambil data dari session
    let dataObj = JSON.parse(sessionStorage.getItem('dataList') || '{}'); // Ambil objek dari session
    let dataList = dataObj.data || []; // Ambil array dari properti "data" atau sebuah array kosong jika properti "data" tidak ada di objek 

    const data = dataList.find((data: any) => data.username === id);
    console.log(data)
    this.form.patchValue({
      username: data.username,
      firstName: data.firstName,
      lastName: data.lastName,
      email: data.email,
      birthDate: data.birthDate,
      basicSalary: data.basicSalary,
      status: data.status,
      group: data.group,
      age:data.age,
      description: data.description
    });
  }

}
