import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { NgbNavChangeEvent } from '@ng-bootstrap/ng-bootstrap';
import { MessageService } from 'primeng/api';
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss'],
  providers: [MessageService],
})
export class IndexComponent implements OnInit {
  public create:string;
  public detail:string;
  constructor(
    private messageService: MessageService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe({
      next: (params) => {
       this.create = params.create,
       this.detail = params.detail
      },
    });
  }
}
