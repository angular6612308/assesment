import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment';
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private REST_API_SERVER = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getProfile$(): Observable<any> {
    return this.http.get(this.REST_API_SERVER + 'profile/basic');
  }
}
