import { HttpClient, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { environment } from "../../../../environments/environment";
import { AuthenticationService } from "../../authentication/authentication.service";

@Injectable({
  providedIn: "root",
})
export class CrudService {
  private REST_API_SERVER = environment.apiUrl;
  private router: Router;

  constructor(
    private http: HttpClient,
    private route: Router,
    private auth: AuthenticationService
  ) {
    this.router = route;
  }

  getAll$(): Observable<any> {
    let params = new HttpParams();
    let userId: any = this.auth.gettUserId();

    // params=params.append('user_id', userId);
    // params=params.append('url', this.router.url);
    let urlTree = this.router.parseUrl(this.router.url);
    urlTree.queryParams = {};
    urlTree.fragment = null; // optional

    let body = {
      // user_id: userId,
      url: urlTree.toString().substring(1),
    };

    // return this.http.get(this.REST_API_SERVER + 'crud', { params })
    return this.http.post(this.REST_API_SERVER + "crud", body);
  }
}
