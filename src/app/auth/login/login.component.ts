import { Component, OnInit } from '@angular/core';
import { UntypedFormBuilder, UntypedFormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
// import { CoreConfigService } from '@core/services/config.service';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';
import { Subject } from 'rxjs';
import {MessageService} from 'primeng/api';
import { PrimeNGConfig } from 'primeng/api';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [MessageService]
})
export class LoginComponent implements OnInit {

  //  Public
  public coreConfig: any;
  public loginForm: UntypedFormGroup;
  public loading = false;
  public submitted = false;
  public returnUrl: string;
  public error = '';
  public passwordTextType: boolean;

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(
    private _formBuilder: UntypedFormBuilder,
    private _route: ActivatedRoute,
    private _router: Router,
    private _authenticationService: AuthenticationService,
    private messageService: MessageService, private primengConfig: PrimeNGConfig
  ) {
    // redirect to home kalo udah login
    if (this._authenticationService.isLoggedIn()) {
      this._router.navigate(['admin']);
    }

    this._unsubscribeAll = new Subject();
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.loginForm.controls;
  }

  togglePasswordTextType() {
    this.passwordTextType = !this.passwordTextType;
  }

  onSubmit() {
    this.submitted = true;

    // Validasi Form Invalid
    if (this.loginForm.invalid) {
      return;
    }

    // Login
    this.loading = true;
    // Ceritanya nembak API untuk login
    setTimeout(() => {
        if(this.loginForm.controls['email'].value == 'weibbek@spacehotline.com' && this.loginForm.controls['password'].value == '12345678' ){
              // Jika berhasil maka masukin token yang didapat dari API
              // Token ini digunakan untuk Bearer Token pada Header untuk validation setiap Hit API
              this._authenticationService.setToken('asdasdss1232');

              // Return ke halaman ini maka otomatis langsung ke dashboard
              this._router.navigate([this.returnUrl]);
          
        }else{
          // Username dan Password salah 
          this.messageService.add({severity:'error', summary: 'Login Failed', detail: 'Username dan Password salah '});
          this.loading = false;
        }
    }, 600);
    
  
    
  }

  // Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.loginForm = this._formBuilder.group({
      email: ['weibbek@spacehotline.com', [Validators.required, Validators.email]],
      password: ['12345678', Validators.required]
    });

    this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || 'admin';
  }
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    // this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

}
